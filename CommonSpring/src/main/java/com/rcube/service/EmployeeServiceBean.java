package com.rcube.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.rcube.model.Employee;

@Service
public class EmployeeServiceBean implements EmployeeService {

	private static final Logger logger = LoggerFactory
			.getLogger(EmployeeServiceBean.class);

	public List<Employee> getAllEmployees() {

		logger.debug("Inside Employee Service");

		return getCollectiveEmpoyees();
	}

	// Helper method to get employees bundle
	private List<Employee> getCollectiveEmpoyees() {
		Employee employeeOne = new Employee();
		employeeOne.setEmployeeAge("22");
		employeeOne.setEmployeeId("1212");
		employeeOne.setEmployeeName("Ramesh");
		employeeOne.setEmployeeWork("PE");
		Employee employeeTwo = new Employee();
		employeeTwo.setEmployeeAge("23");
		employeeTwo.setEmployeeId("2121");
		employeeTwo.setEmployeeName("Shan");
		employeeTwo.setEmployeeWork("BA");
		Employee employeeThree = new Employee();
		employeeThree.setEmployeeAge("24");
		employeeThree.setEmployeeId("2211");
		employeeThree.setEmployeeName("Naresh");
		employeeThree.setEmployeeWork("TL");
		Employee employeeFour = new Employee();
		employeeFour.setEmployeeAge("23");
		employeeFour.setEmployeeId("2422");
		employeeFour.setEmployeeName("Ashwin");
		employeeFour.setEmployeeWork("ARCH");

		List<Employee> employeeList = Arrays.asList(employeeOne, employeeTwo,
				employeeThree, employeeFour);
		return employeeList;
	}

}
