package com.rcube.service;

import java.util.List;

import com.rcube.model.Employee;

public interface EmployeeService {

	List<Employee> getAllEmployees();

}
