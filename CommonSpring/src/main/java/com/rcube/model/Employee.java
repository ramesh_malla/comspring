package com.rcube.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Employee {

	@JsonProperty
	private String employeeId;
	@JsonProperty
	private String employeeName;
	@JsonProperty
	private String employeeAge;
	@JsonProperty
	private String employeeWork;

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(String employeeAge) {
		this.employeeAge = employeeAge;
	}

	public String getEmployeeWork() {
		return employeeWork;
	}

	public void setEmployeeWork(String employeeWork) {
		this.employeeWork = employeeWork;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((employeeAge == null) ? 0 : employeeAge.hashCode());
		result = prime * result
				+ ((employeeId == null) ? 0 : employeeId.hashCode());
		result = prime * result
				+ ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result
				+ ((employeeWork == null) ? 0 : employeeWork.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (employeeAge == null) {
			if (other.employeeAge != null)
				return false;
		} else if (!employeeAge.equals(other.employeeAge))
			return false;
		if (employeeId == null) {
			if (other.employeeId != null)
				return false;
		} else if (!employeeId.equals(other.employeeId))
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (employeeWork == null) {
			if (other.employeeWork != null)
				return false;
		} else if (!employeeWork.equals(other.employeeWork))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName="
				+ employeeName + ", employeeAge=" + employeeAge
				+ ", employeeWork=" + employeeWork + "]";
	}

}
