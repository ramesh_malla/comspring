package com.rcube.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rcube.model.Employee;
import com.rcube.service.EmployeeService;

/**
 * 
 * @author Ramesh
 * 
 * @info deploy this app and access using
 *       http://localhost:6080/CommonSpring/employee/getEmployee --can change
 *       the port
 */
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeServiceBean;

	private static final Logger logger = LoggerFactory
			.getLogger(EmployeeController.class);

	@RequestMapping(value = "/getEmployee")
	public List<Employee> getAllEmployee() {
		logger.info("Inside Employee Controller");

		return employeeServiceBean.getAllEmployees();
	}

}
