package com.rcube.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rcube.service.EmployeeService;

public class EmployeeMainClass {
	public static void main(String[] args) {
		// If you are here this is one way of loading beans into application
		// context without server's help. Just a simple main class
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"apContext.xml");

		EmployeeService es = applicationContext.getBean(EmployeeService.class);

		System.out.println(es.getAllEmployees());

	}

}
