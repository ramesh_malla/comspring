package com.rcube.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rcube.model.EmployeeListVO;
import com.rcube.model.EmployeeVO;

@RestController
public class EmployeeRESTController {
	@RequestMapping(value = "/employees")
	public List<EmployeeVO> getAllEmployees() {
		EmployeeListVO employees = new EmployeeListVO();

		EmployeeVO empOne = new EmployeeVO(1, "Lokesh", "Gupta",
				"xxxxx@gmail.com");
		EmployeeVO empTwo = new EmployeeVO(2, "Amit", "Singhal",
				"yyyyyy@yahoo.com");
		EmployeeVO empThree = new EmployeeVO(3, "Kirti", "Mishra",
				"zzzzzz@gmail.com");

		List<EmployeeVO> empl = new ArrayList<EmployeeVO>();

		empl.add(empOne);
		empl.add(empTwo);
		empl.add(empThree);

		return empl;
	}

	@RequestMapping(value = "/employees/{id}")
	@ResponseBody
	public ResponseEntity<EmployeeVO> getEmployeeById(@PathVariable("id") int id) {
		if (id <= 3) {
			EmployeeVO employee = new EmployeeVO(1, "Lokesh", "Gupta",
					"ssssss@gmail.com");
			return new ResponseEntity<EmployeeVO>(employee, HttpStatus.OK);
		}
		return new ResponseEntity(HttpStatus.NOT_FOUND);
	}
}